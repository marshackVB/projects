# Deep Learning for Text  
This is a work in progress.  

So far I have built a [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) script to launch and configure an EMR cluster. The cluster serves a Pyspark-powered Jupyter Notebook. I am planning to process the text data using the [NLP library for Spark](https://nlp.johnsnowlabs.com/). The data will be stored in S3.  

I also built a boto3 script to launch a GPU-backed Jupyter Notebook using Amazon's Deep Learning AMI.
