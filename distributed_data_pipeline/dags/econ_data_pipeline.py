import requests, zipfile, io, yaml, os, sys
from datetime import datetime, timedelta
from io import StringIO
import airflow
from airflow import DAG
from airflow.operators import PythonOperator, PostgresOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.hooks import PostgresHook

# Add project path to system path so that project moduels
# can be imported
file_directory = os.path.dirname(os.path.realpath(__file__))
parent_directory = os.path.dirname(file_directory)
sys.path.append(parent_directory)

# Custome modules
from utilities import utilities
from config.sources_config import qcew
from sources.qcew.query_definitions import create_qcew

# Instantiate a DAG
default_args = {'owner': 'Airflow',
                'depends_on_past': False,
                'start_date': airflow.utils.dates.days_ago(2),
                'retries': 1,
                'retry_delay': timedelta(minutes=1)}

dag = DAG('econ_data_pipeline', default_args=default_args,
          schedule_interval='@once')


def create_postgres_qcew_table():
    hook = PostgresHook('qcew_tables')
    cur = hook.get_conn().cursor()
    cur.execute("DROP TABLE IF EXISTS qcew;")
    cur.execute(create_qcew)


def qcew_download_and_unzip(year, temp_storage_folder):
    """Download and unzip a qcew file
    """

    r = requests.get('https://data.bls.gov/cew/data/files/{0}/csv/{0}_qtrly_by_area.zip'.format(year))
    z = zipfile.ZipFile(io.BytesIO(r.content))

    z.extractall(temp_storage_folder)


def qcew_csv_to_postgres(year, temp_storage_folder):
    """Move a collection of csv files into a postgres database
    """

    hook = PostgresHook('qcew_tables')

    year_map = {2017: '2017.q1-q4.by_area/',
                2018: '2018.q1-q2.by_area/'}

    folder = temp_storage_folder + year_map[year]

    for file in os.listdir(folder):
        full_file_path = folder + file
        #with open(folder + file, 'r') as f:
            #csv = f.read().decode('ascii', 'ignore')
        hook.copy_expert("""COPY qcew FROM STDIN WITH (FORMAT CSV, HEADER True)""", full_file_path)




create_postgres_table = PythonOperator(task_id= "create_postgres_qcew_table",
                                        python_callable=create_postgres_qcew_table,
                                        dag=dag)


# Tasks to ingest multiple qcew zip files
ingest_csv = {}
ingest_postgres = {}

for period in qcew['periods']:
    ingest_csv[period] = PythonOperator(task_id = 'qcew_download_{}'.format(period),
                                python_callable=qcew_download_and_unzip,
                                op_kwargs={'year': period, 'temp_storage_folder': qcew['temp_storage_folder']},
                                dag=dag)


    ingest_postgres[period] = PythonOperator(task_id = 'qcew_to_postgres_{}'.format(period),
                                     python_callable=qcew_csv_to_postgres,
                                     op_kwargs={'year': period, 'temp_storage_folder': qcew['temp_storage_folder']},
                                     dag=dag)


    create_postgres_table.set_downstream(ingest_csv[period])
    ingest_postgres[period].set_upstream(ingest_csv[period])



group_postgres_writes = DummyOperator(task_id = 'group_postgres_writes',
                                      dag=dag)

for task in ingest_postgres.values():
    group_postgres_writes.set_upstream(task)

msa_indicators = PostgresOperator(task_id = 'create_msa_table',
                                sql=["DROP TABLE IF EXISTS qcew_metro;",
                                  """CREATE TABLE qcew_metro AS
                                   SELECT *
                                   FROM (SELECT year,
                                          qtr,
                                          area_title,
                                          own_title,
                                          industry_title,
                                          agglvl_title,
                                          avg_wkly_wage,
                                          month1_emplvl,
                                          month2_emplvl,
                                          month3_emplvl,
                                          CASE WHEN agglvl_title LIKE 'MSA%' THEN 'MSA'
                                               WHEN agglvl_title LIKE 'MicroSA%' THEN 'MICRO'
                                               WHEN agglvl_title LIKE 'CMSA%' THEN 'CMSA'
                                               WHEN agglvl_title LIKE 'County%' THEN 'COUNTY'
                                               WHEN agglvl_title LIKE 'State%' THEN 'STATE'
                                               WHEN agglvl_title LIKE 'Total%' THEN 'TOTAL'
                                               WHEN agglvl_title LIKE 'National%' THEN 'NATIONAL'
                                          ELSE '0'
                                          END as geo_type
                                   FROM qcew
                                   WHERE own_title = 'Total Covered') as s
                                   WHERE geo_type = 'MSA'
                                   ORDER BY area_title, year, qtr;"""],
                                   postgres_conn_id="qcew_tables",
                                   autocommit=False,
                                   parameters=None,
                                   database='qcew',
                                   dag=dag)


msa_indicators.set_upstream(group_postgres_writes)
