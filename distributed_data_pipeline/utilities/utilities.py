def get_binned_index(list_of_files, n_bins):
    """Create index slice intervals to slice a list into
    n_bins sublists

    Example:
        Original list: ['a', 'b', 'c', 'd']
        Function return with n_bins = 2: [(0, 2), (2,)]
    """
    index_cutpoint = int(len(list_of_files)/n_bins)

    def bin_index(n_bins, index_start, index_end):
        if n_bins == 1:
            return [(index_start,)]
        else:
            return [(index_start, index_end)] + bin_index(n_bins -1, index_end, index_end + index_cutpoint)

    return bin_index(n_bins, 0, index_cutpoint)
