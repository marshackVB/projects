"""
Different configurations related to the various data sources ingested by
the DAG
"""

qcew = {'periods': [2017, 2018],
        'temp_storage_folder': '/home/marshall/Documents/DataScience/projects/distributed_data_pipeline/sources/temp_files/',
        'n_tasks': 4}
