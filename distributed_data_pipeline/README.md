# A Distributed Data Pipeline with Airflow and Celery  
This is a work in progress.  

My vision for this project is to design a distributed [Apache Airflow](https://airflow.apache.org/) data pipeline that extracts various regional economic series (employment, wages, GDP, etc.) from the web, stores them in a postgresql database and performs various transformations. The pipeline will then power a Bokeh dashboard application served from an EC2 instance.  

The pipeline can be easily distributed on a single machine using Airflow's LocalExecutor execution mode, which relies on Python's multiprocessing. I will eventually implement the pipeline with CeleryExector mode, which utilizes [Celery](http://www.celeryproject.org/) workers and could allow the pipeline to scale to multiple machines.


### Useful resources:  
#### Setup:  
 - [Airflow docs: installation](https://airflow.apache.org/installation.html)
 - [Installing Apache Airflow on Ubuntu/AWS](https://medium.com/a-r-g-o/installing-apache-airflow-on-ubuntu-aws-6ebac15db211)  
 - [Apache Airflow Installation on Ubuntu](https://medium.com/@taufiq_ibrahim/apache-airflow-installation-on-ubuntu-ddc087482c14)   
 - [Building a Data Pipeline with Airflow](https://tech.marksblogg.com/airflow-postgres-redis-forex.html )  
 - [ETL Pipelines with Airflow](http://michael-harmon.com/blog/AirflowETL.html#first-bullet)

#### Usage:  
 - [Airflow docs: Tutorial](https://airflow.apache.org/tutorial.html)
 - [A Practical Introduction to Airflow (video)](https://www.youtube.com/watch?v=cHATHSB_450)  
 - [Orielly data pipeline course repository](https://github.com/kjam/data-pipelines-course)  
