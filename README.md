# A repository of Data Science projects  

This repository contains example projects I am working on as well as other useful information related to Data Science.

**data_matching**: Data matching using active machine learning and parallel processing with [Dask](https://dask.org/)  

**choropleth**: Creating an interactive choropleth map using [Bokeh](https://bokeh.pydata.org/en/latest/)  

**deeplearning_for_text** (In progress): Text analytics using [Apache Spark's NLP library](https://nlp.johnsnowlabs.com/) and Deep Learning with [Keras](https://keras.io/)  

**distributed_data_pipeline** (In progress): A distributed data processing pipeline using [Apache Airflow](https://airflow.apache.org/index.html) and [Celery](http://www.celeryproject.org/)
